package nolinks

//language=PostgreSQL
var tableCreateScript = `CREATE TABLE IF NOT EXISTS nolinks__permitted_users (
		platform text not null,
		channel text not null,
		user_id text not null,
		perma bool default false not null,
		constraint nolinks__permitted_users_pk
			primary key (platform, channel, user_id)
	);
	
	CREATE TABLE IF NOT EXISTS nolinks__toggle (
		platform text not null,
		channel text not null,
		disabled bool default false not null,
		constraint nolinks__toggle_pk
			primary key (platform, channel)
	);
	
	CREATE TABLE IF NOT EXISTS nolinks__toggle_timeout (
		platform text not null,
		channel text not null,
		enabled bool default false not null,
		constraint nolinks__toggle_timeout_pk
			primary key (platform, channel)
	);

	CREATE TABLE IF NOT EXISTS nolinks__timeout_conf
	(
		platform       text not null,
		channel        text not null,
		warn_duration  int  not null,
		warn_count     int  not null,
		timeout_length int not null,
		constraint nolinks__timeout_conf_pk
			primary key (platform, channel)
	);
	
	ALTER TABLE nolinks__timeout_conf ADD COLUMN IF NOT EXISTS message text;`

type nlTimeoutConfig struct {
	WarningDuration int `db:"warn_duration"`
	WarningCount    int `db:"warn_count"`
	TimeoutLength   int `db:"timeout_length"`
	Message         *string
}
