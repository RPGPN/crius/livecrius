package nolinks

import (
	"bytes"
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	twitchIRC "github.com/gempir/go-twitch-irc/v2"
	"github.com/go-redis/redis/v8"
	"github.com/jmoiron/sqlx"
	CriusUtils "gitlab.com/RPGPN/crius-utils"
	"gitlab.com/RPGPN/crius-utils/LiveCrius"
	cc "gitlab.com/RPGPN/criuscommander/v2"
	goleshchat "gitlab.com/ponkey364/golesh-chat"
	"net/http"
	"strconv"
	"strings"
	"text/template"
	"time"
)

var defaultWarningMessage = "That's warning {{.WarningsGiven}} of {{.WarningCount}} for linking"

func (n *NoLinks) DisableTimeouts(m *cc.MessageContext, _ []string) error {
	channel := m.RealmID

	platform := CriusUtils.GetPlatform(m.Context)

	switch platform {
	case cc.PlatformTwitch:
		{
			message := m.PlatformData.(*twitchIRC.PrivateMessage)
			if !LiveCrius.TwitchHasPermission(message) {
				m.Send("you are not permitted to do this.")
				return nil
			}
		}
	case cc.PlatformGlimesh:
		{
			message := m.PlatformData.(*goleshchat.ChatMessage)
			if !LiveCrius.GlimeshHasPermission(message) {
				m.Send("you are not permitted to do this.")
				return nil
			}
		}
	}

	noLinksDisabled, err := isNoLinksDisabled(n.db, platform, channel)
	if err != nil {
		return err
	}

	if *noLinksDisabled {
		m.Send("NoLinks (and therefore Timeouts) is already disabled.")
		return nil
	}

	var enabled bool
	err = n.db.Get(&enabled, "SELECT enabled FROM nolinks__toggle_timeout WHERE platform=$1 AND channel=$2",
		platform.ToString(), channel)
	if err != nil && err != sql.ErrNoRows {
		return err
	}

	if !enabled || err == sql.ErrNoRows {
		m.Send("Timeouts are already off")
		return nil
	}

	_, err = n.db.Exec(`INSERT INTO nolinks__toggle_timeout as nltt (platform, channel, enabled) VALUES ($1, $2, false)
		ON CONFLICT ON CONSTRAINT nolinks__toggle_timeout_pk DO UPDATE SET enabled=false WHERE nltt.platform=$1 AND nltt.channel=$2`,
		platform.ToString(), channel)
	if err != nil {
		return err
	}

	m.Send("Timeouts disabled")

	return nil
}

func (n *NoLinks) EnableTimeouts(m *cc.MessageContext, args []string) error {
	if len(args) < 3 {
		m.Send("Usage: `timeouts on [warning count] [warning duration] [short/long timeout] [optional: message]`")
		return nil
	}

	channel := m.RealmID
	platform := CriusUtils.GetPlatform(m.Context)

	switch platform {
	case cc.PlatformTwitch:
		{
			message := m.PlatformData.(*twitchIRC.PrivateMessage)
			if !LiveCrius.TwitchHasPermission(message) {
				m.Send("you are not permitted to do this.")
				return nil
			}
		}
	case cc.PlatformGlimesh:
		{
			message := m.PlatformData.(*goleshchat.ChatMessage)
			if !LiveCrius.GlimeshHasPermission(message) {
				m.Send("you are not permitted to do this.")
				return nil
			}
		}
	}

	noLinksDisabled, err := isNoLinksDisabled(n.db, platform, channel)
	if err != nil {
		return err
	}

	if *noLinksDisabled {
		m.Send("NoLinks has to be enabled for Timeouts to be configured.")
		return nil
	}

	var enabled bool
	err = n.db.Get(&enabled, "SELECT enabled FROM nolinks__toggle_timeout WHERE platform=$1 AND channel=$2",
		platform.ToString(), channel)
	if err != nil && err != sql.ErrNoRows {
		return err
	}

	if enabled {
		m.Send("Timeouts are already enabled")
		return nil
	}

	warnDur, err := time.ParseDuration(args[1])
	if err != nil {
		return err
	}

	warnCount, err := strconv.Atoi(args[0])
	if err != nil {
		return err
	}

	var timeoutLength time.Duration
	switch strings.ToLower(args[2]) {
	case "short":
		timeoutLength = time.Minute * 5
	case "long":
		timeoutLength = time.Minute * 15
	default:
		m.Send("invalid timeout length")
		return nil
	}

	var message *string
	if len(args) == 4 {
		message = &args[3]
	}

	// first, set the config. this upserts because someone might turn it off and then on again.
	_, err = n.db.Exec(`INSERT INTO nolinks__timeout_conf (platform, channel, warn_duration, warn_count, timeout_length, message)
		VALUES ($1, $2, $3, $4, $5, $6)
		ON CONFLICT ON CONSTRAINT nolinks__timeout_conf_pk
		DO UPDATE SET warn_duration=$3, warn_count=$4, timeout_length=$5, message=$6`,
		platform.ToString(), channel, warnDur.Milliseconds(), warnCount, timeoutLength.Milliseconds(), message)
	if err != nil {
		return err
	}

	// now enable timeouts
	_, err = n.db.Exec(`INSERT INTO nolinks__toggle_timeout as nltt (platform, channel, enabled) VALUES ($1, $2, true)
		ON CONFLICT ON CONSTRAINT nolinks__toggle_timeout_pk DO UPDATE SET enabled=true WHERE nltt.platform=$1 AND nltt.channel=$2`,
		platform.ToString(), channel)
	if err != nil {
		return err
	}

	m.Send("Timeouts enabled")

	return nil
}

func (n *NoLinks) SetTimeout(m *cc.MessageContext, args []string) error {
	if len(args) == 0 {
		m.Send("Usage: `timeouts settimeout [short/long timeout]`")
		return nil
	}

	channel := m.RealmID
	platform := CriusUtils.GetPlatform(m.Context)

	switch platform {
	case cc.PlatformTwitch:
		{
			message := m.PlatformData.(*twitchIRC.PrivateMessage)
			if !LiveCrius.TwitchHasPermission(message) {
				m.Send("you are not permitted to do this.")
				return nil
			}
		}
	case cc.PlatformGlimesh:
		{
			message := m.PlatformData.(*goleshchat.ChatMessage)
			if !LiveCrius.GlimeshHasPermission(message) {
				m.Send("you are not permitted to do this.")
				return nil
			}
		}
	}

	noLinksDisabled, err := isNoLinksDisabled(n.db, platform, channel)
	if err != nil {
		return err
	}

	if *noLinksDisabled {
		m.Send("NoLinks has to be enabled for Timeouts to be configured.")
		return nil
	}

	var enabled bool
	err = n.db.Get(&enabled, "SELECT enabled FROM nolinks__toggle_timeout WHERE platform=$1 AND channel=$2",
		platform.ToString(), channel)
	if err != nil && err != sql.ErrNoRows {
		return err
	}

	if !enabled || err == sql.ErrNoRows {
		m.Send("Timeouts are disabled")
		return nil
	}

	var timeoutLength time.Duration
	switch strings.ToLower(args[0]) {
	case "short":
		timeoutLength = time.Minute * 5
	case "long":
		timeoutLength = time.Minute * 15
	default:
		m.Send("invalid timeout length")
		return nil
	}

	_, err = n.db.Exec("UPDATE nolinks__timeout_conf SET timeout_length=$1 WHERE platform=$2 AND channel=$3",
		timeoutLength.Milliseconds(), platform.ToString(), channel)
	if err != nil {
		return err
	}

	m.Send("Set the timeout length to " + args[0] + " (" + timeoutLength.String() + ")")

	return nil
}

func (n *NoLinks) SetWarnDuration(m *cc.MessageContext, args []string) error {
	if len(args) == 0 {
		m.Send("Usage: `timeouts setwarndur [dur]` (i.e. 15m / 1h / 1h30m)")
		return nil
	}

	channel := m.RealmID
	platform := CriusUtils.GetPlatform(m.Context)

	switch platform {
	case cc.PlatformTwitch:
		{
			message := m.PlatformData.(*twitchIRC.PrivateMessage)
			if !LiveCrius.TwitchHasPermission(message) {
				m.Send("you are not permitted to do this.")
				return nil
			}
		}
	case cc.PlatformGlimesh:
		{
			message := m.PlatformData.(*goleshchat.ChatMessage)
			if !LiveCrius.GlimeshHasPermission(message) {
				m.Send("you are not permitted to do this.")
				return nil
			}
		}
	}

	noLinksDisabled, err := isNoLinksDisabled(n.db, platform, channel)
	if err != nil {
		return err
	}

	if *noLinksDisabled {
		m.Send("NoLinks has to be enabled for Timeouts to be configured.")
		return nil
	}

	var enabled bool
	err = n.db.Get(&enabled, "SELECT enabled FROM nolinks__toggle_timeout WHERE platform=$1 AND channel=$2",
		platform.ToString(), channel)
	if err != nil && err != sql.ErrNoRows {
		return err
	}

	if !enabled || err == sql.ErrNoRows {
		m.Send("Timeouts are disabled")
		return nil
	}

	warnDur, err := time.ParseDuration(args[0])
	if err != nil {
		return err
	}

	_, err = n.db.Exec("UPDATE nolinks__timeout_conf SET warn_duration=$1 WHERE platform=$2 AND channel=$3",
		warnDur.Milliseconds(), platform.ToString(), channel)
	if err != nil {
		return err
	}

	m.Send("Set the warning duration to " + args[0] + " (" + warnDur.String() + ")")

	return nil
}

func (n *NoLinks) SetWarnCount(m *cc.MessageContext, args []string) error {
	if len(args) == 0 {
		m.Send("Usage `timeouts setwarncount [count]`")
		return nil
	}

	channel := m.RealmID
	platform := CriusUtils.GetPlatform(m.Context)

	switch platform {
	case cc.PlatformTwitch:
		{
			message := m.PlatformData.(*twitchIRC.PrivateMessage)
			if !LiveCrius.TwitchHasPermission(message) {
				m.Send("you are not permitted to do this.")
				return nil
			}
		}
	case cc.PlatformGlimesh:
		{
			message := m.PlatformData.(*goleshchat.ChatMessage)
			if !LiveCrius.GlimeshHasPermission(message) {
				m.Send("you are not permitted to do this.")
				return nil
			}
		}
	}

	noLinksDisabled, err := isNoLinksDisabled(n.db, platform, channel)
	if err != nil {
		return err
	}

	if *noLinksDisabled {
		m.Send("NoLinks has to be enabled for Timeouts to be configured.")
		return nil
	}

	var enabled bool
	err = n.db.Get(&enabled, "SELECT enabled FROM nolinks__toggle_timeout WHERE platform=$1 AND channel=$2",
		platform.ToString(), channel)
	if err != nil && err != sql.ErrNoRows {
		return err
	}

	if !enabled || err == sql.ErrNoRows {
		m.Send("Timeouts are disabled")
		return nil
	}

	warnCount, err := strconv.Atoi(args[0])
	if err != nil {
		return err
	}

	_, err = n.db.Exec("UPDATE nolinks__timeout_conf SET warn_count=$1 WHERE platform=$2 AND channel=$3",
		warnCount, platform.ToString(), channel)
	if err != nil {
		return err
	}

	m.Send("Set the warning count to " + args[0])

	return nil
}

func (n *NoLinks) SetWarnMessage(m *cc.MessageContext, args []string) error {
	channel := m.RealmID
	platform := CriusUtils.GetPlatform(m.Context)

	switch platform {
	case cc.PlatformTwitch:
		{
			message := m.PlatformData.(*twitchIRC.PrivateMessage)
			if !LiveCrius.TwitchHasPermission(message) {
				m.Send("you are not permitted to do this.")
				return nil
			}
		}
	case cc.PlatformGlimesh:
		{
			message := m.PlatformData.(*goleshchat.ChatMessage)
			if !LiveCrius.GlimeshHasPermission(message) {
				m.Send("you are not permitted to do this.")
				return nil
			}
		}
	}

	noLinksDisabled, err := isNoLinksDisabled(n.db, platform, channel)
	if err != nil {
		return err
	}

	if *noLinksDisabled {
		m.Send("NoLinks has to be enabled for Timeouts to be configured.")
		return nil
	}

	var enabled bool
	err = n.db.Get(&enabled, "SELECT enabled FROM nolinks__toggle_timeout WHERE platform=$1 AND channel=$2",
		platform.ToString(), channel)
	if err != nil && err != sql.ErrNoRows {
		return err
	}

	if !enabled || err == sql.ErrNoRows {
		m.Send("Timeouts are disabled")
		return nil
	}

	var message *string
	if len(args) > 0 {
		// let's not bother with the whole escaping spaces stuff here and just join everything
		joinedMessage := strings.Join(args, " ")
		message = &joinedMessage
	}

	_, err = n.db.Exec("UPDATE nolinks__timeout_conf SET message=$1 WHERE platform=$2 AND channel=$3",
		message, platform.ToString(), channel)
	if err != nil {
		return err
	}

	m.Send("Set the warning message")

	return nil
}

func areTimeoutsEnabled(db *sqlx.DB, platform cc.PlatformType, channel string) (bool, error) {
	var enabled bool
	err := db.Get(&enabled, "SELECT enabled FROM nolinks__toggle_timeout WHERE platform=$1 AND channel=$2",
		platform.ToString(), channel)
	if err != nil && err != sql.ErrNoRows {
		return false, err
	}

	if !enabled || err == sql.ErrNoRows {
		return false, nil
	}

	return true, nil
}

func doWarning(db *sqlx.DB, redisStore *redis.Client, m *cc.MessageContext, userID string, channel string) error {
	platform := CriusUtils.GetPlatform(m.Context)

	key := fmt.Sprintf("nlWarn:%s>%s.%s", platform.ToString(), channel, userID)

	// let's have us some config
	config := &nlTimeoutConfig{}
	err := db.Get(config, "SELECT timeout_length, warn_count, warn_duration, message FROM nolinks__timeout_conf WHERE platform=$1 AND channel=$2",
		platform.ToString(), channel)
	if err != nil {
		return err
	}

	warnings, err := redisStore.Incr(context.Background(), key).Result()
	if err != nil {
		return err
	}

	pWarningsGiven.WithLabelValues(platform.ToString()).Inc()

	if int(warnings) >= config.WarningCount {
		timeoutDur := time.Duration(int(time.Millisecond) * config.TimeoutLength)

		// time them out
		switch platform {
		case cc.PlatformGlimesh:
			glimeshTimeout(userID, channel, timeoutDur, m.Context)
		case cc.PlatformTwitch:
			m.Send(fmt.Sprintf("/timeout %s %s posting links", m.Author.Username, timeoutDur))
		}

		// now remove the warnings from redis
		redisStore.Del(context.Background(), key)

		return nil
	}

	// set the ttl on the redis key to config.WarningDuration
	// WarningDuration is stored in ms so we have to * by ms since Duration uses ns.
	dur := time.Duration(config.WarningDuration * int(time.Millisecond))
	err = redisStore.Expire(context.Background(), key, dur).Err()
	if err != nil {
		return err
	}

	if config.Message == nil {
		config.Message = &defaultWarningMessage
	}

	t, err := template.New("").Parse(*config.Message)
	if err != nil {
		return err
	}

	var buf bytes.Buffer

	err = t.Execute(&buf, map[string]interface{}{
		"WarningsGiven": warnings,
		"WarningCount":  config.WarningCount,
	})
	if err != nil {
		return err
	}

	m.Send(buf.String())

	return nil
}

func glimeshTimeout(userID string, channelID string, duration time.Duration, ctx context.Context) error {
	api := LiveCrius.GetAPIFuncs(ctx)

	var mutName string
	if duration == time.Minute*5 {
		mutName = "shortTimeoutUser"
	} else if duration == time.Minute*15 {
		mutName = "longTimeoutUser"
	}

	// we request action here just because it's the first thing and we don't care apart from that it's successful!
	query := fmt.Sprintf("mutation ($channelID: ID!, $userID: ID!) {\n  %s(channelId: $channelID, userId: $userID) {\n    action\n  }\n}\n", mutName)

	bodydata, err := json.Marshal(&struct {
		Query     string            `json:"query"`
		Variables map[string]string `json:"variables"`
	}{
		Query: query,
		Variables: map[string]string{
			"channelID": channelID,
			"userID":    userID,
		},
	})
	if err != nil {
		return err
	}

	req, err := http.NewRequest(http.MethodPost, glimeshAPI, bytes.NewReader(bodydata))
	if err != nil {
		return err
	}

	_, err = api.MakeGlimeshReq(req, false)
	if err != nil {
		return err
	}

	return nil
}
