package nolinks

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"github.com/asaskevich/EventBus"
	twitchIRC "github.com/gempir/go-twitch-irc/v2"
	"github.com/go-redis/redis/v8"
	"github.com/jmoiron/sqlx"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/sirupsen/logrus"
	CriusUtils "gitlab.com/RPGPN/crius-utils"
	"gitlab.com/RPGPN/crius-utils/LiveCrius"
	cc "gitlab.com/RPGPN/criuscommander/v2"
	"gitlab.com/RPGPN/criuscommander/v2/glimesh"
	"gitlab.com/RPGPN/criuscommander/v2/twitch"
	goleshchat "gitlab.com/ponkey364/golesh-chat"
	"regexp"
)

const (
	twitchAPIRoot = "https://api.twitch.tv/helix"
	glimeshAPI    = "https://glimesh.tv/api"
)

var (
	pWarningsGiven *prometheus.GaugeVec
	pLinksRemoved  *prometheus.GaugeVec
)

var PluginInfo = &cc.PluginJSON{
	Name:               "NoLinks",
	License:            "MPL-2.0",
	Creator:            "Ben <Ponkey364>",
	URL:                "https://gitlab.com/RPGPN/livecrius/-/tree/hosted/plugins/nolinks",
	Description:        "Plugin that blocks links in chat unless the streamer (or their moderators) allow it",
	SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh,
	Commands: []*cc.PJCommand{
		{
			Name:               "NoLinks",
			Activator:          "nolinks",
			Help:               "Commands for controlling NoLinks",
			SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh,
			Subcommands: []*cc.PJCommand{
				{
					Name:               "Disable NoLinks",
					Activator:          "off",
					HandlerName:        "off",
					SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh,
					Help:               "Disable the removal of links. (off by default)",
				},
				{
					Name:               "Enable NoLinks",
					Activator:          "on",
					HandlerName:        "on",
					SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh,
					Help:               "Enable the removal of links (off by default).",
				},
				{
					Name:               "Allow Link Once",
					Activator:          "allowonce",
					Help:               "Allow the specified user to send one link",
					SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh,
					HandlerName:        "allowonce",
				},
				{
					Name:               "Allow User",
					Activator:          "allowuser",
					Help:               "Allow the specified user to send links permanently (until removed)",
					SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh,
					HandlerName:        "allowuser",
				},
				{
					Name:               "Disallow User",
					Activator:          "disallowuser",
					Help:               "Remove the specified user's permission to send links",
					SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh,
					HandlerName:        "disallowuser",
				},
			},
		},
		{
			Name:               "Timeouts",
			Activator:          "timeouts",
			HandlerName:        "timeouts",
			Help:               "Commands for controlling timeouts",
			SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh,
			Subcommands: []*cc.PJCommand{
				{
					Name:               "Enable Timeouts",
					Activator:          "on",
					HandlerName:        "on",
					SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh,
					Help:               "Enable timing out users for posting too many links in a specified time (off by default). Usage: `timeouts on [warning count] [warning duration] [short/long timeout] [optional: message]`",
				},
				{
					Name:               "Set Timeout",
					Activator:          "settimeout",
					HandlerName:        "settimeout",
					SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh,
					Help:               "Set how long to time users out for, short or long. Usage: `timeouts settimeout [short/long]`",
				},
				{
					Name:               "Set Warning Duration",
					Activator:          "setwarndur",
					HandlerName:        "setwarndur",
					SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh,
					Help:               "How long should a warning last for before being reset. Usage `timeouts setwarndur [dur]` (i.e. 15m / 1h / 1h30m)",
				},
				{
					Name:               "Set Warning Count",
					Activator:          "setwarncount",
					HandlerName:        "setwarncount",
					SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh,
					Help:               "How many warnings should I give a user before timing them out. Usage `timeouts setwarncount [count]`",
				},
				{
					Name:               "Set Warning Message",
					Activator:          "setwarnmsg",
					HandlerName:        "setwarnmsg",
					SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh,
					Help:               "What message should I send to tell a user they've got a warning (blank=crius default) (please read the docs for more). Usage: `timeouts setwarnmsg [message]`",
				},
				{
					Name:               "Disable Timeouts",
					Activator:          "off",
					HandlerName:        "off",
					SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh,
					Help:               "Disable timing out users for posting too many links in a specified time (off by default)",
				},
			},
		},
	},
}

var linkRegex = regexp.MustCompile("[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@:%_\\+.~#?&//=]*)")

func Setup(ctx context.Context) (map[string]cc.CommandHandler, error) {
	// get stuff we need from ctx
	s := LiveCrius.GetSettings(ctx)
	api := LiveCrius.GetAPIFuncs(ctx)
	bus := ctx.Value("bus").(EventBus.BusSubscriber)

	db := s.GetDB()

	// run the table creation script from db.go
	// create a transaction
	tx := db.MustBegin()

	// run the script
	_, err := tx.Exec(tableCreateScript)
	if err != nil {
		return nil, err
	}

	// done
	err = tx.Commit()
	if err != nil {
		return nil, err
	}

	// prometheus
	pWarningsGiven = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: "livecrius",
		Subsystem: "nolinks",
		Name:      "warnings_given",
		Help:      "Number of warnings given to users about links, partitioned by platform",
	}, []string{"platform"})

	pLinksRemoved = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: "livecrius",
		Subsystem: "nolinks",
		Name:      "links_removed",
		Help:      "Number of links removed, partitioned by platform",
	}, []string{"platform"})

	nl := &NoLinks{
		db:         db,
		redisStore: s.GetRedisStore(),
		api:        api,
	}

	// register event handlers
	bus.Subscribe(glimesh.EventMessageCreate, nl.GlimeshMessageSendEvent)
	bus.Subscribe(twitch.EventPrivateMessage, nl.TwitchMessageSendEvent)

	return map[string]cc.CommandHandler{
		"nolinks/allowonce":     nl.AllowLinkOnce,
		"nolinks/allowuser":     nl.AllowUserLink,
		"nolinks/disallowuser":  nl.DisallowUserLink,
		"nolinks/off":           nl.DisableNoLinks,
		"nolinks/on":            nl.EnableNoLinks,
		"timeouts/on":           nl.EnableTimeouts,
		"timeouts/settimeout":   nl.SetTimeout,
		"timeouts/setwarndur":   nl.SetWarnDuration,
		"timeouts/off":          nl.DisableTimeouts,
		"timeouts/setwarncount": nl.SetWarnCount,
		"timeouts/setwarnmsg":   nl.SetWarnMessage,
	}, nil
}

type NoLinks struct {
	db *sqlx.DB
	// redis (db1/store) is used to store how many warnings we've given someone
	redisStore *redis.Client
	api        LiveCrius.APIFuncs
}

func (n *NoLinks) DisableNoLinks(m *cc.MessageContext, _ []string) error {
	channel := m.RealmID

	platform := CriusUtils.GetPlatform(m.Context)

	switch platform {
	case cc.PlatformTwitch:
		{
			if !LiveCrius.TwitchHasPermission(m.PlatformData.(*twitchIRC.PrivateMessage)) {
				m.Send("you are not permitted to do this.")
				return nil
			}
		}
	case cc.PlatformGlimesh:
		{
			message := m.PlatformData.(*goleshchat.ChatMessage)
			if !LiveCrius.GlimeshHasPermission(message) {
				m.Send("you are not permitted to do this.")
				return nil
			}
		}
	}

	var disabled bool
	err := n.db.Get(&disabled, "SELECT disabled FROM nolinks__toggle WHERE platform=$1 AND channel=$2",
		platform.ToString(), channel)
	if err != nil && err != sql.ErrNoRows {
		return err
	}

	if disabled || errors.Is(err, sql.ErrNoRows) {
		return nil
	}

	_, err = n.db.Exec(`INSERT INTO nolinks__toggle as nlt (platform, channel, disabled) VALUES ($1, $2, TRUE)
		ON CONFLICT ON CONSTRAINT nolinks__toggle_pk DO UPDATE SET disabled=TRUE WHERE nlt.platform=$1 AND nlt.channel=$2`,
		platform.ToString(), channel)
	if err != nil {
		return err
	}

	m.Send("NoLinks disabled")

	return nil
}

func (n *NoLinks) EnableNoLinks(m *cc.MessageContext, _ []string) error {
	channel := m.RealmID

	platform := CriusUtils.GetPlatform(m.Context)

	switch platform {
	case cc.PlatformTwitch:
		{
			if !LiveCrius.TwitchHasPermission(m.PlatformData.(*twitchIRC.PrivateMessage)) {
				m.Send("you are not permitted to do this.")
				return nil
			}
		}
	case cc.PlatformGlimesh:
		{
			message := m.PlatformData.(*goleshchat.ChatMessage)
			if !LiveCrius.GlimeshHasPermission(message) {
				m.Send("you are not permitted to do this.")
				return nil
			}
		}
	}

	var disabled bool
	err := n.db.Get(&disabled, "SELECT disabled FROM nolinks__toggle WHERE platform=$1 AND channel=$2",
		platform.ToString(), channel)
	if err != nil && err != sql.ErrNoRows {
		return err
	}

	if !disabled && err != sql.ErrNoRows {
		return nil
	}

	_, err = n.db.Exec(`INSERT INTO nolinks__toggle as nlt (platform, channel, disabled) VALUES ($1, $2, false)
		ON CONFLICT ON CONSTRAINT nolinks__toggle_pk DO UPDATE SET disabled=false WHERE nlt.platform=$1 AND nlt.channel=$2`,
		platform.ToString(), channel)
	if err != nil {
		return err
	}

	m.Send("NoLinks enabled")

	return nil
}

func (n *NoLinks) AllowLinkOnce(m *cc.MessageContext, args []string) error {
	var userID string

	channel := m.RealmID

	platform := CriusUtils.GetPlatform(m.Context)

	// is nolinks disabled in this channel?
	isDisabled, err := isNoLinksDisabled(n.db, platform, channel)
	if err != nil {
		return err
	}

	if *isDisabled {
		m.Send("NoLinks is disabled. You can enable it with `nolinkson`")
		return nil
	}

	switch platform {
	case cc.PlatformTwitch:
		{
			message := m.PlatformData.(*twitchIRC.PrivateMessage)

			if !LiveCrius.TwitchHasPermission(message) {
				m.Send("you are not permitted to do this.")
				return nil
			}

			id, err := getTwitchIDFromName(args[0], n.api)
			if err != nil {
				return err
			}

			userID = *id
		}
	case cc.PlatformGlimesh:
		{
			message := m.PlatformData.(*goleshchat.ChatMessage)
			if !LiveCrius.GlimeshHasPermission(message) {
				m.Send("you are not permitted to do this.")
				return nil
			}

			id, err := getGlimeshIDFromName(args[0], n.api)
			if err != nil {
				return err
			}

			userID = *id
		}
	}

	_, err = n.db.Exec(`INSERT INTO nolinks__permitted_users (platform, channel, user_id, perma)
		VALUES ($1, $2, $3, FALSE)`, platform.ToString(), channel, userID)
	if err != nil {
		return err
	}

	m.Send(fmt.Sprintf("%s is permitted for one link", args[0]))

	return nil
}

func (n *NoLinks) AllowUserLink(m *cc.MessageContext, args []string) error {
	var userID string

	channel := m.RealmID

	platform := CriusUtils.GetPlatform(m.Context)

	// is nolinks disabled in this channel?
	isDisabled, err := isNoLinksDisabled(n.db, platform, m.RealmID)
	if err != nil {
		return err
	}

	if *isDisabled {
		m.Send("NoLinks is disabled. You can enable it with `nolinkson`")
		return nil
	}

	switch platform {
	case cc.PlatformTwitch:
		{
			message := m.PlatformData.(*twitchIRC.PrivateMessage)

			if !LiveCrius.TwitchHasPermission(message) {
				m.Send("you are not permitted to do this.")
				return nil
			}

			id, err := getTwitchIDFromName(args[0], n.api)
			if err != nil {
				return err
			}

			userID = *id
		}
	case cc.PlatformGlimesh:
		{
			message := m.PlatformData.(*goleshchat.ChatMessage)
			if !LiveCrius.GlimeshHasPermission(message) {
				m.Send("you are not permitted to do this.")
				return nil
			}

			id, err := getGlimeshIDFromName(args[0], n.api)
			if err != nil {
				return err
			}

			userID = *id
		}
	}

	_, err = n.db.Exec(`INSERT INTO nolinks__permitted_users (platform, channel, user_id, perma)
		VALUES ($1, $2, $3, TRUE)`, platform.ToString(), channel, userID)
	if err != nil {
		return err
	}

	m.Send(fmt.Sprintf("%s is permitted for *all* the links", args[0]))

	return nil
}

func (n *NoLinks) DisallowUserLink(m *cc.MessageContext, args []string) error {
	var userID string

	channel := m.RealmID

	platform := CriusUtils.GetPlatform(m.Context)

	// is nolinks disabled in this channel?
	isDisabled, err := isNoLinksDisabled(n.db, platform, m.RealmID)
	if err != nil {
		return err
	}

	if *isDisabled {
		m.Send("NoLinks is disabled. You can enable it with `nolinkson`")
		return nil
	}

	switch platform {
	case cc.PlatformTwitch:
		{
			message := m.PlatformData.(*twitchIRC.PrivateMessage)

			if !LiveCrius.TwitchHasPermission(message) {
				m.Send("you are not permitted to do this.")
				return nil
			}

			id, err := getTwitchIDFromName(args[0], n.api)
			if err != nil {
				return err
			}

			userID = *id
		}
	case cc.PlatformGlimesh:
		{
			message := m.PlatformData.(*goleshchat.ChatMessage)
			if !LiveCrius.GlimeshHasPermission(message) {
				m.Send("you are not permitted to do this.")
				return nil
			}

			id, err := getGlimeshIDFromName(args[0], n.api)
			if err != nil {
				return err
			}

			userID = *id
		}
	}

	_, err = n.db.Exec(`DELETE FROM nolinks__permitted_users WHERE platform=$1 AND channel=$2 AND user_id=$3`,
		platform.ToString(), channel, userID)
	if err != nil {
		return err
	}

	m.Send(fmt.Sprintf("Removed %s's link permission", args[0]))

	return nil
}

func isNoLinksDisabled(db *sqlx.DB, platform cc.PlatformType, channel string) (*bool, error) {
	var disabled bool
	err := db.Get(&disabled, "SELECT disabled FROM nolinks__toggle WHERE platform=$1 AND channel=$2",
		platform.ToString(), channel)
	if err != nil && err != sql.ErrNoRows {
		return nil, err
	}

	if disabled || errors.Is(err, sql.ErrNoRows) {
		t := true //lol
		return &t, nil
	}

	f := false
	return &f, nil
}

type dbRow struct {
	Platform string
	Channel  string
	Perma    bool
	UserID   string `db:"user_id"`
}

func (n *NoLinks) GlimeshMessageSendEvent(m *cc.MessageContext) {
	data := m.PlatformData.(*goleshchat.ChatMessage)

	// is nolinks disabled in this channel?
	isDisabled, err := isNoLinksDisabled(n.db, cc.PlatformGlimesh, m.RealmID)
	if err != nil {
		logrus.Error(err)
		return
	}

	if *isDisabled {
		return
	}

	// did we send the message?
	conf := CriusUtils.GetOtherConfig(m.Context)
	if m.Author.ID == conf["GlimeshID"].(string) {
		return
	}

	content := data.Message

	if testForLink(content) {
		// found a link
		// we should check if the sender is clear.
		perm := &dbRow{}

		err := n.db.Get(perm, "SELECT * FROM nolinks__permitted_users WHERE platform='GLIMESH' AND channel=$1 AND user_id=$2", m.RealmID, m.Author.ID)
		if err == sql.ErrNoRows {
			// delete the message
			err := glimeshDeleteMessage(n.api, m.RealmID, data.ID)
			if err != nil {
				logrus.Error(err)
				return
			}

			pLinksRemoved.WithLabelValues(cc.PlatformGlimesh.ToString()).Inc()

			// what we can do however, is warn them and timeout
			timeoutsEnabled, err := areTimeoutsEnabled(n.db, cc.PlatformGlimesh, m.RealmID)
			if err != nil {
				logrus.Error(err)
				return
			}

			if timeoutsEnabled {
				err := doWarning(n.db, n.redisStore, m, m.Author.ID, m.RealmID)
				if err != nil {
					logrus.Error(err)
					return
				}
			}

			return
		}
		if err != nil {
			logrus.Error(err)
			return
		}

		// they're fine.
		// are they perma?
		if perm.Perma {
			return
		}

		// delete their entry
		_, err = n.db.Exec("DELETE FROM nolinks__permitted_users WHERE platform='GLIMESH' AND channel=$1 AND user_id=$2", perm.Channel, perm.UserID)
		if err != nil {
			logrus.Error(err)
			return
		}
	}
}

func (n *NoLinks) TwitchMessageSendEvent(m *cc.MessageContext) {
	data := m.PlatformData.(*twitchIRC.PrivateMessage)

	// is nolinks disabled in this channel?
	isDisabled, err := isNoLinksDisabled(n.db, cc.PlatformTwitch, m.RealmID)
	if err != nil {
		logrus.Error(err)
		return
	}

	if *isDisabled {
		return
	}

	// did we send the message?
	conf := CriusUtils.GetOtherConfig(m.Context)
	if m.Author.ID == conf["TwitchName"].(string) {
		return
	}

	// but was it sent by the broadcaster or a mod?
	if LiveCrius.TwitchHasPermission(data) {
		// yes, leave them alone.
		return
	}

	content := data.Message

	if testForLink(content) {
		// found a link
		// we should check if the sender is clear.
		perm := &dbRow{}

		err := n.db.Get(perm, "SELECT * FROM nolinks__permitted_users WHERE platform='TWITCH' AND channel=$1 AND user_id=$2", m.RealmID, m.Author.ID)
		if err == sql.ErrNoRows {
			// delete the message
			_, err := m.Send(fmt.Sprintf("/delete %s", data.ID))
			if err != nil {
				logrus.Error(err)
			}

			pLinksRemoved.WithLabelValues(cc.PlatformTwitch.ToString()).Inc()

			timeoutsEnabled, err := areTimeoutsEnabled(n.db, cc.PlatformTwitch, m.RealmID)
			if err != nil {
				logrus.Error(err)
				return
			}

			if timeoutsEnabled {
				err := doWarning(n.db, n.redisStore, m, m.Author.ID, m.RealmID)
				if err != nil {
					logrus.Error(err)
					return
				}
			}

			return
		}
		if err != nil {
			logrus.Error(err)
			return
		}

		// they're fine.
		// are they perma?
		if perm.Perma {
			return
		}

		// delete their entry
		_, err = n.db.Exec("DELETE FROM nolinks__permitted_users WHERE platform='TWITCH' AND channel=$1 AND user_id=$2", perm.Channel, perm.UserID)
		if err != nil {
			logrus.Error(err)
			return
		}
	}
}

func testForLink(data string) bool {
	return linkRegex.Match([]byte(data))
}
