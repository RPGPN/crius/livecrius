package nolinks

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitlab.com/RPGPN/crius-utils/LiveCrius"
	cc "gitlab.com/RPGPN/criuscommander/v2"
	"io/ioutil"
	"net/http"
	"time"
)

func getTwitchIDFromName(name string, api LiveCrius.APIFuncs) (*string, error) {
	// silently ignores the cache param
	cached, hit, err := api.CheckCache(cc.PlatformTwitch, "idcache", name, nil)
	if err != nil {
		return nil, err
	}
	if hit {
		return cached, nil
	}

	req, err := http.NewRequest(http.MethodGet, fmt.Sprintf("%s/users?login=%s", twitchAPIRoot, name), nil)
	if err != nil {
		return nil, err
	}

	res, err := api.MakeTwitchReq(req)
	if err != nil {
		return nil, err
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	data := &struct {
		Data []struct {
			ID string
		}
	}{}

	err = json.Unmarshal(body, data)
	if err != nil {
		return nil, err
	}

	// silently ignores the cache param
	err = api.AddToCache(cc.PlatformTwitch, "idcache", name, data.Data[0].ID, nil, time.Hour*72)
	if err != nil {
		return nil, err
	}

	return &data.Data[0].ID, nil
}

func getGlimeshIDFromName(name string, api LiveCrius.APIFuncs) (*string, error) {
	// silently ignores the cache param
	cached, hit, err := api.CheckCache(cc.PlatformGlimesh, "idcache", name, nil)
	if err != nil {
		return nil, err
	}
	if hit {
		return cached, nil
	}

	const query = "query ($username: String) {\n  user(username: $username) {\n    id\n  }\n}\n"

	bodydata, err := json.Marshal(&struct {
		Query     string            `json:"query"`
		Variables map[string]string `json:"variables"`
	}{
		Query: query,
		Variables: map[string]string{
			"username": name,
		},
	})
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest(http.MethodPost, glimeshAPI, bytes.NewReader(bodydata))
	if err != nil {
		return nil, err
	}

	res, err := api.MakeGlimeshReq(req, false)
	if err != nil {
		return nil, err
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	data := &struct {
		Data struct {
			User struct {
				ID string
			}
		}
	}{}

	err = json.Unmarshal(body, data)
	if err != nil {
		return nil, err
	}

	// silently ignores the cache param
	err = api.AddToCache(cc.PlatformGlimesh, "idcache", name, data.Data.User.ID, nil, time.Hour*72)
	if err != nil {
		return nil, err
	}

	return &data.Data.User.ID, nil
}

func glimeshDeleteMessage(api LiveCrius.APIFuncs, channelID string, messageID string) error {
	bodydata, err := json.Marshal(&struct {
		Query     string            `json:"query"`
		Variables map[string]string `json:"variables"`
	}{
		Query: "mutation ($channelID: ID!, $messageID: ID!) {\n  deleteMessage(channelId: $channelID, messageId: $messageID) {\n    insertedAt\n  }\n}",
		Variables: map[string]string{
			"channelID": channelID,
			"messageID": messageID,
		},
	})

	req, err := http.NewRequest(http.MethodPost, glimeshAPI, bytes.NewReader(bodydata))
	if err != nil {
		return err
	}

	_, err = api.MakeGlimeshReq(req, false)
	if err != nil {
		return err
	}

	return nil
}
