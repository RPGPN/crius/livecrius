package timers

const tableCreateScript = `CREATE TABLE IF NOT EXISTS timers__timers
(
    realm_id     text not null,
    platform     text not null,
    name         text not null,
    time         int  not null,
    min_messages int  not null,
    message      text not null,
    constraint timers__timers_pk primary key (realm_id, platform, name)
);`

type timer struct {
	MinMessages uint   `db:"min_messages"`
	Message     string `db:"message"`
}
