package timers

import (
	"context"
	"fmt"
	twitchIRC "github.com/gempir/go-twitch-irc/v2"
	"github.com/go-co-op/gocron"
	"github.com/go-redis/redis/v8"
	"github.com/jmoiron/sqlx"
	"github.com/sirupsen/logrus"
	CriusUtils "gitlab.com/RPGPN/crius-utils"
	"gitlab.com/RPGPN/crius-utils/LiveCrius"
	cc "gitlab.com/RPGPN/criuscommander/v2"
	goleshchat "gitlab.com/ponkey364/golesh-chat"
	"sync"
	"time"
)

var PluginInfo = &cc.PluginJSON{
	Name:               "Timers",
	License:            "MPL-2.0",
	Creator:            "Ben <Ponkey364>",
	URL:                "https://gitlab.com/RPGPN/livecrius/-/tree/hosted/plugins/timers",
	Description:        "Message Timers",
	SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh,
	Commands: []*cc.PJCommand{
		{
			Name:               "New Timer",
			HandlerName:        "newtimer",
			Activator:          "newtimer",
			Help:               "Add a new timer. Usage: `newtimer [time (e.g. 5m / 1h / 45m] [name] [message]`",
			SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh,
		},
		{
			Name:               "Remove Timer",
			HandlerName:        "rmtimer",
			Activator:          "rmtimer",
			Help:               "Remove a timer. Usage: `rmtimer [name]`",
			SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh,
		},
	},
}

type jobs struct {
	Jobs map[string]*gocron.Job
	*sync.Mutex
}

type timers struct {
	db         *sqlx.DB
	cron       *gocron.Scheduler
	jobs       *jobs
	redisStore *redis.Client
}

func Setup(ctx context.Context) (map[string]cc.CommandHandler, error) {
	// get stuff we need from ctx
	s := LiveCrius.GetSettings(ctx)

	db := s.GetDB()
	redisStore := s.GetRedisStore()

	// run the table creation script from db.go
	// create a transaction
	tx := db.MustBegin()

	// run the script
	_, err := tx.Exec(tableCreateScript)
	if err != nil {
		return nil, err
	}

	// done
	err = tx.Commit()
	if err != nil {
		return nil, err
	}

	cron := gocron.NewScheduler(time.UTC)

	t := &timers{
		db:   db,
		cron: cron,
		jobs: &jobs{
			Jobs:  make(map[string]*gocron.Job),
			Mutex: &sync.Mutex{},
		},
		redisStore: redisStore,
	}

	err = t.startAllTimers(ctx)
	if err != nil {
		return nil, err
	}

	cron.StartAsync()

	return map[string]cc.CommandHandler{
		"newtimer": t.NewTimer,
		"rmtimer":  t.RemTimer,
	}, nil
}

func (t *timers) NewTimer(m *cc.MessageContext, args []string) error {
	if len(args) < 3 {
		m.Send("Usage: `newtimer [time (e.g. 5m / 1h / 45m] [name] [message]`")
		return nil
	}

	channel := m.RealmID

	platform := CriusUtils.GetPlatform(m.Context)

	switch platform {
	case cc.PlatformTwitch:
		{
			if !LiveCrius.TwitchHasPermission(m.PlatformData.(*twitchIRC.PrivateMessage)) {
				m.Send("you are not permitted to do this.")
				return nil
			}
		}
	case cc.PlatformGlimesh:
		{
			message := m.PlatformData.(*goleshchat.ChatMessage)
			if !LiveCrius.GlimeshHasPermission(message) {
				m.Send("you are not permitted to do this.")
				return nil
			}
		}
	}

	//minMessages, err := strconv.Atoi(args[1])
	//if err != nil {
	//	return err
	//}

	dur, err := time.ParseDuration(args[0])
	if err != nil {
		return err
	}

	_, err = t.db.Exec(`INSERT INTO timers__timers (realm_id, platform, name, time, min_messages, message)
		VALUES ($1, $2, $3, $4, $5, $6)`,
		channel, platform.ToString(), args[1], dur.Seconds(), 0, args[2])
	if err != nil {
		return err
	}

	job, err := t.cron.Every(dur).Do(doTimer(m.Context, platform, m.RealmID, t.db, t.redisStore, args[1]))
	if err != nil {
		return err
	}

	inStoreName := fmt.Sprintf("%s:%s.%s", platform.ToString(), channel, args[1])

	t.jobs.Lock()
	if running, ok := t.jobs.Jobs[inStoreName]; ok {
		// this already exists?1
		// remove it and we'll add it again.
		t.cron.RemoveByReference(running)
	}

	t.jobs.Jobs[inStoreName] = job
	t.jobs.Unlock()

	return nil
}

func (t *timers) RemTimer(m *cc.MessageContext, args []string) error {
	if len(args) == 0 {
		m.Send("Usage: `rmtimer [name]`")
		return nil
	}

	channel := m.RealmID

	platform := CriusUtils.GetPlatform(m.Context)

	switch platform {
	case cc.PlatformTwitch:
		{
			if !LiveCrius.TwitchHasPermission(m.PlatformData.(*twitchIRC.PrivateMessage)) {
				m.Send("you are not permitted to do this.")
				return nil
			}
		}
	case cc.PlatformGlimesh:
		{
			message := m.PlatformData.(*goleshchat.ChatMessage)
			if !LiveCrius.GlimeshHasPermission(message) {
				m.Send("you are not permitted to do this.")
				return nil
			}
		}
	}

	_, err := t.db.Exec("DELETE FROM timers__timers WHERE realm_id=$1 AND platform=$2 AND name=$3", channel,
		platform.ToString(), args[0])
	if err != nil {
		return err
	}

	inStoreName := fmt.Sprintf("%s:%s.%s", platform.ToString(), channel, args[0])

	t.jobs.Lock()
	if running, ok := t.jobs.Jobs[inStoreName]; ok {
		t.cron.RemoveByReference(running)
		delete(t.jobs.Jobs, inStoreName)
	}
	t.jobs.Unlock()

	m.Send("Removed timer " + args[0])

	return nil
}

func doTimer(ctx context.Context, platform cc.PlatformType, channel string, db *sqlx.DB, redisStore *redis.Client, name string) func() {
	return func() {
		commander := ctx.Value("commander").(*cc.Commander)

		// whats 1) the message and 2) the amnt of min. new msgs
		thisTimer := &timer{}
		err := db.Get(thisTimer, "SELECT message, min_messages FROM timers__timers WHERE platform=$1 AND realm_id=$2 AND name=$3",
			platform.ToString(), channel, name)
		if err != nil {
			logrus.Error(err)
			return
		}

		// this'll be true for now until I get this working.
		shouldFire := true
		//if thisTimer.MinMessages > 0 {
		//	redisStore.HGet(context.Background(), fmt.Sprintf("timers_%s:messages", platform.ToString()))
		//}

		if shouldFire {
			commander.SendMessage(platform, channel, thisTimer.Message)
		}
	}
}

func (t *timers) startAllTimers(ctx context.Context) error {
	// get a list of all the timers
	var timers []*struct {
		RealmID  string `db:"realm_id"`
		Platform string
		Name     string
		Time     int64
	}

	err := t.db.Select(&timers, "SELECT realm_id, platform, name, time FROM timers__timers")
	if err != nil {
		return err
	}

	for _, timer := range timers {
		var ccPlatform cc.PlatformType
		switch timer.Platform {
		case cc.PlatformTwitch.ToString():
			ccPlatform = cc.PlatformTwitch
		case cc.PlatformGlimesh.ToString():
			ccPlatform = cc.PlatformGlimesh
		}

		dur := time.Duration(timer.Time * int64(time.Second))

		job, err := t.cron.Every(dur).Do(doTimer(ctx, ccPlatform, timer.RealmID, t.db, t.redisStore, timer.Name))
		if err != nil {
			return err
		}

		inStoreName := fmt.Sprintf("%s:%s.%s", timer.Platform, timer.RealmID, timer.Name)

		t.jobs.Lock()
		t.jobs.Jobs[inStoreName] = job
		t.jobs.Unlock()
	}

	return nil
}
