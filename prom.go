package main

import (
	"context"
	"github.com/asaskevich/EventBus"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	CriusUtils "gitlab.com/RPGPN/crius-utils"
	cc "gitlab.com/RPGPN/criuscommander/v2"
	"gitlab.com/RPGPN/criuscommander/v2/glimesh"
	"gitlab.com/RPGPN/criuscommander/v2/twitch"
)

var PromPluginInfo = &cc.PluginJSON{
	Name:               "Prometheus Metrics",
	License:            "MPL-2.0",
	Creator:            "Ben <Ponkey364>",
	URL:                "https://gitlab.com/RPGPN/livecrius/-/tree/hosted/prom",
	Description:        "Prometheus metrics plugin",
	SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh,
	Commands:           []*cc.PJCommand{},
}

// This plugin just does misc prometheus stuff
func PromSetup(ctx context.Context) (map[string]cc.CommandHandler, error) {
	bus := ctx.Value("bus").(EventBus.BusSubscriber)
	commander := CriusUtils.GetCommander(ctx)

	pMessagesSeen := promauto.NewCounterVec(prometheus.CounterOpts{
		Namespace: "livecrius",
		Subsystem: "commander",
		Name:      "messages_seen",
		Help:      "Number of messages seen, partitioned by platform",
	}, []string{"platform"})

	pValidCommandsExecd := promauto.NewCounterVec(prometheus.CounterOpts{
		Namespace: "livecrius",
		Subsystem: "commander",
		Name:      "commands_valid_execd",
		Help:      "Number of valid commands ran, partitioned by platform & command",
	}, []string{"platform", "command"})

	pInvalidCommandsExecd := promauto.NewCounterVec(prometheus.CounterOpts{
		Namespace: "livecrius",
		Subsystem: "commander",
		Name:      "commands_invalid_execd",
		Help:      "Number of invalid commands attempted, partitioned by platform",
	}, []string{"platform"})

	// message handlers so we can report how many messages we've seen
	bus.Subscribe(glimesh.EventMessageCreate, func(m *cc.MessageContext) {
		pMessagesSeen.WithLabelValues(CriusUtils.GetPlatform(m.Context).ToString()).Inc()
	})
	bus.Subscribe(twitch.EventPrivateMessage, func(m *cc.MessageContext) {
		pMessagesSeen.WithLabelValues(CriusUtils.GetPlatform(m.Context).ToString()).Inc()
	})

	commander.GuardBeforeCommandExec = func(m *cc.MessageContext, cmd string, _ []string) bool {
		platform := CriusUtils.GetPlatform(m.Context)
		pValidCommandsExecd.WithLabelValues(platform.ToString(), cmd).Inc()
		return true
	}

	commander.GuardPostCommandCheck = func(m *cc.MessageContext, _ string, _ []string) {
		platform := CriusUtils.GetPlatform(m.Context)
		pInvalidCommandsExecd.WithLabelValues(platform.ToString()).Inc()
	}

	return nil, nil
}
