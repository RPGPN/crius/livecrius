package main

import (
	crius_plugin_botinfo "gitlab.com/RPGPN/crius-plugin-botinfo"
	crius_plugin_info "gitlab.com/RPGPN/crius-plugin-info"
	crius_plugin_links "gitlab.com/RPGPN/crius-plugin-links"
	crius_plugin_polls "gitlab.com/RPGPN/crius-plugin-polls"
	crius_plugin_quotes "gitlab.com/RPGPN/crius-plugin-quotes"
	crius_plugin_raffle "gitlab.com/RPGPN/crius-plugin-raffle"
	"gitlab.com/RPGPN/crius/livecrius/plugins/nolinks"
	"gitlab.com/RPGPN/crius/livecrius/plugins/timers"
	"gitlab.com/RPGPN/crius/livecrius/settings/channelmanager"
	cc "gitlab.com/RPGPN/criuscommander/v2"
	"os"
	"strings"
)

func loadPlugins(cmder *cc.Commander) error {
	// should we load the channelmanager?
	if strings.ToLower(os.Getenv("CHANNEL_MANAGER")) == "true" {
		if err := cmder.SetupPluginFromConfig(channelmanager.Setup, channelmanager.PluginInfo); err != nil {
			return err
		}
	}

	if err := cmder.SetupPluginFromConfig(PromSetup, PromPluginInfo); err != nil {
		return err
	}

	if err := cmder.SetupPluginFromConfig(crius_plugin_botinfo.Setup, crius_plugin_botinfo.PluginInfo); err != nil {
		return err
	}

	if err := cmder.SetupPluginFromConfig(nolinks.Setup, nolinks.PluginInfo); err != nil {
		return err
	}

	if err := cmder.SetupPluginFromConfig(crius_plugin_links.Setup, crius_plugin_links.PluginInfo); err != nil {
		return err
	}

	if err := cmder.SetupPluginFromConfig(crius_plugin_raffle.Setup, crius_plugin_raffle.PluginInfo); err != nil {
		return err
	}

	if err := cmder.SetupPluginFromConfig(crius_plugin_info.Setup, crius_plugin_info.PluginInfo); err != nil {
		return err
	}

	if err := cmder.SetupPluginFromConfig(crius_plugin_quotes.Setup, crius_plugin_quotes.PluginInfo); err != nil {
		return err
	}

	if err := cmder.SetupPluginFromConfig(crius_plugin_polls.Setup, crius_plugin_polls.PluginInfo); err != nil {
		return err
	}

	if err := cmder.SetupPluginFromConfig(timers.Setup, timers.PluginInfo); err != nil {
		return err
	}

	return nil
}
