package main

import (
	"context"
	"encoding/json"
	"github.com/sirupsen/logrus"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/endpoints"
	"io/ioutil"
	"os"
	"path"
)

type tokenFileFmt struct {
	Tokens map[string]*oauth2.Token
	Codes  map[string]string
}

var (
	tokens map[string]*oauth2.Token // where the key is the platform name
	codes  map[string]string        // oauth2 codes that we use for getting a token

	twitchConf  *oauth2.Config
	glimeshConf *oauth2.Config
)

func loadTokensFile(file *os.File) error {
	// load the JSON
	fileData, err := ioutil.ReadAll(file)
	if err != nil {
		return err
	}

	data := &tokenFileFmt{}

	err = json.Unmarshal(fileData, &data)
	if err != nil {
		return err
	}

	tokens = data.Tokens
	codes = data.Codes

	return nil
}

func saveTokensFile() error {
	file, err := os.OpenFile(path.Join(os.Getenv("DATA_DIR"), "tokens.json"), os.O_TRUNC|os.O_WRONLY, os.ModePerm)
	if err != nil {
		// is Go causing FS problems again?
		panic(err)
	}

	data, err := json.Marshal(&tokenFileFmt{
		Tokens: tokens,
		Codes:  codes,
	})
	if err != nil {
		return err
	}

	_, err = file.Write(data)
	if err != nil {
		return err
	}

	return nil
}

func init() {
	// load or create the tokens file
	file, err := os.OpenFile(path.Join(os.Getenv("DATA_DIR"), "tokens.json"), os.O_CREATE|os.O_RDONLY, os.ModePerm)
	if err != nil {
		// is Go causing FS problems again?
		panic(err)
	}

	stat, err := file.Stat()
	if err != nil {
		panic(err)
	}

	if stat.Size() != 0 {
		err = loadTokensFile(file)
		if err != nil {
			panic(err)
		}
	} else {
		tokens = make(map[string]*oauth2.Token)
		codes = make(map[string]string)
	}

	file.Close()

	twitchConf = &oauth2.Config{
		ClientID:     os.Getenv("TWITCH_CID"),
		ClientSecret: os.Getenv("TWITCH_SEC"),
		Endpoint:     endpoints.Twitch,
		RedirectURL:  os.Getenv("TWITCH_RED_URI"),
		Scopes:       []string{"channel:moderate", "chat:edit", "chat:read", "moderation:read"},
	}

	glimeshConf = &oauth2.Config{
		ClientID:     os.Getenv("GLIM_CID"),
		ClientSecret: os.Getenv("GLIM_SEC"),
		Endpoint: oauth2.Endpoint{
			AuthURL:   "https://glimesh.tv/oauth/authorize",
			TokenURL:  "https://glimesh.tv/api/oauth/token",
			AuthStyle: oauth2.AuthStyleInParams,
		},
		RedirectURL: os.Getenv("GLIM_RED_URI"),
		Scopes:      []string{"chat"},
	}
}

func GetTwitchAccessToken() (*string, error) {
	if token, ok := tokens["twitch"]; ok {
		if token.Valid() {
			// this token hasn't expired, return it
			return &token.AccessToken, nil
		}

		// it's invalid, refresh
		tSrc := twitchConf.TokenSource(context.Background(), token)
		t, err := tSrc.Token()
		if err != nil {
			return nil, err
		}

		tokens["twitch"] = t
		err = saveTokensFile()
		if err != nil {
			return nil, err
		}

		return &t.AccessToken, nil
	}
	// we don't have a token, do we at least have a code?
	if code, ok := codes["twitch"]; ok {
		// yes, we should do the last bit of oauth
		token, err := twitchConf.Exchange(context.Background(), code)
		if err != nil {
			return nil, err
		}

		// pop the token in the right place
		tokens["twitch"] = token

		// and delete the key
		delete(codes, "twitch")

		err = saveTokensFile()
		if err != nil {
			return nil, err
		}

		return &token.AccessToken, nil
	}

	logrus.Warn("No Twitch Tokens or Codes, doing OAuth")
	logrus.Infof("Please go to %s to complete setup, then put the code into tokens.json under codes.twitch",
		twitchConf.AuthCodeURL("state"))

	// make codes.twitch to help the user
	codes["twitch"] = "YOUR_CODE_HERE"

	err := saveTokensFile()
	if err != nil {
		return nil, err
	}

	os.Exit(401)

	return nil, nil
}

func GetGlimeshAccessToken() (*string, error) {
	if token, ok := tokens["glimesh"]; ok {
		if token.Valid() {
			// this token hasn't expired, return it
			return &token.AccessToken, nil
		}

		// it's invalid, refresh
		tSrc := glimeshConf.TokenSource(context.Background(), token)
		t, err := tSrc.Token()
		if err != nil {
			return nil, err
		}

		tokens["glimesh"] = t
		err = saveTokensFile()
		if err != nil {
			return nil, err
		}

		return &t.AccessToken, nil
	}
	// we don't have a token, do we at least have a code?
	if code, ok := codes["glimesh"]; ok {
		// yes, we should do the last bit of oauth
		token, err := glimeshConf.Exchange(context.Background(), code)
		if err != nil {
			return nil, err
		}

		// pop the token in the right place
		tokens["glimesh"] = token

		// and delete the key
		delete(codes, "glimesh")

		err = saveTokensFile()
		if err != nil {
			return nil, err
		}

		return &token.AccessToken, nil
	}

	logrus.Warn("No Glimesh Tokens or Codes, doing OAuth")
	logrus.Infof("Please go to %s to complete setup, then put the code into tokens.json under codes.glimesh",
		glimeshConf.AuthCodeURL("state"))

	// make codes.twitch to help the user
	codes["glimesh"] = "YOUR_CODE_HERE"

	err := saveTokensFile()
	if err != nil {
		return nil, err
	}

	os.Exit(401)

	return nil, nil
}
