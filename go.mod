module gitlab.com/RPGPN/crius/livecrius

go 1.15

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0 // indirect
	github.com/GuiaBolso/darwin v0.0.0-20191218124601-fd6d2aa3d244
	github.com/asaskevich/EventBus v0.0.0-20200907212545-49d423059eef
	github.com/cznic/ql v1.2.0 // indirect
	github.com/gempir/go-twitch-irc/v2 v2.5.0
	github.com/getsentry/sentry-go v0.10.0
	github.com/go-co-op/gocron v0.7.1
	github.com/go-redis/redis/v8 v8.6.0
	github.com/jmoiron/sqlx v1.3.1
	github.com/joho/godotenv v1.3.0 // indirect
	github.com/lib/pq v1.9.0
	github.com/makasim/sentryhook v0.3.0
	github.com/prometheus/client_golang v1.9.0
	github.com/sirupsen/logrus v1.8.1
	gitlab.com/RPGPN/crius-plugin-botinfo v1.1.2
	gitlab.com/RPGPN/crius-plugin-info v1.2.0
	gitlab.com/RPGPN/crius-plugin-links v1.1.1
	gitlab.com/RPGPN/crius-plugin-polls v1.1.1
	gitlab.com/RPGPN/crius-plugin-quotes v1.1.1
	gitlab.com/RPGPN/crius-plugin-raffle v1.1.1
	gitlab.com/RPGPN/crius-utils v1.5.1
	gitlab.com/RPGPN/criuscommander/v2 v2.4.1
	gitlab.com/ponkey364/golesh-chat v1.1.2
	golang.org/x/oauth2 v0.0.0-20210113205817-d3ed898aa8a3
	golang.org/x/sys v0.0.0-20210403161142-5e06dd20ab57 // indirect
)
