package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"github.com/go-redis/redis/v8"
	cc "gitlab.com/RPGPN/criuscommander/v2"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

// Returns the val in the cache (as a *string, you may need to convert), true if a cache hit, an error
// now silently ignores the cache param
func (api APIFuncs) CheckCache(platform cc.PlatformType, rootKey string, subKey interface{}, _ *redis.Client) (*string, bool, error) {
	key := fmt.Sprintf("%s:%s.%s", rootKey, platform.ToString(), subKey)

	res, err := api.cache.Get(context.Background(), key).Result()
	if err == redis.Nil {
		return nil, false, nil
	}
	if err != nil {
		return nil, false, err
	}

	return &res, true, nil
}

// now silently ignores the cache param
func (api APIFuncs) AddToCache(platform cc.PlatformType, rootKey string, subKey interface{}, val interface{}, _ *redis.Client, ttl time.Duration) error {
	key := fmt.Sprintf("%s:%s.%s", rootKey, platform.ToString(), subKey)
	return api.cache.Set(context.Background(), key, val, ttl).Err()
}

type APIFuncs struct {
	cache *redis.Client
}

func (api APIFuncs) GlimeshGetChannelID(username string) (*int, error) {
	cached, hit, err := api.CheckCache(cc.PlatformGlimesh, "chanid", username, api.cache)
	if err != nil {
		return nil, err
	}
	if hit {
		cachedInt, err := strconv.Atoi(*cached)
		if err != nil {
			return nil, err
		}

		return &cachedInt, nil
	}

	const query = `query ($username: String) {
  channel(username: $username) {
    id
  }
}`

	bodydata, err := json.Marshal(&struct {
		Query     string            `json:"query"`
		Variables map[string]string `json:"variables"`
	}{
		Query: query,
		Variables: map[string]string{
			"username": username,
		},
	})
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest(http.MethodPost, "https://glimesh.tv/api", bytes.NewReader(bodydata))
	if err != nil {
		return nil, err
	}

	res, err := api.MakeGlimeshReq(req, false)
	if err != nil {
		return nil, err
	}

	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	resp := &struct {
		Data struct {
			Channel struct {
				ID string
			}
		}
	}{}

	err = json.Unmarshal(data, resp)
	if err != nil {
		return nil, err
	}

	intID, err := strconv.Atoi(resp.Data.Channel.ID)
	if err != nil {
		return nil, err
	}

	err = api.AddToCache(cc.PlatformGlimesh, "chanid", username, intID, api.cache, time.Hour*72)
	if err != nil {
		return nil, err
	}

	return &intID, nil
}

func (_ APIFuncs) MakeTwitchReq(req *http.Request) (*http.Response, error) {
	token, err := GetTwitchAccessToken()
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	req.Header.Add("Authorization", "Bearer "+*token)
	req.Header.Add("Client-Id", os.Getenv("TWITCH_CID"))
	return client.Do(req)
}

func (_ APIFuncs) MakeGlimeshReq(req *http.Request, rawGraphQL bool) (*http.Response, error) {
	token, err := GetGlimeshAccessToken()
	if err != nil {
		return nil, err
	}

	client := http.Client{}
	req.Header.Add("Authorization", "Bearer "+*token)

	if !rawGraphQL {
		req.Header.Add("content-type", "application/json")
	}
	return client.Do(req)
}

// Find out what our ID is so that we can ignore ourselves and our username so we can join our own channel.
func (api APIFuncs) glimeshWhoAmI() (*string, *string, error) {
	const query = `query {
	  myself {
		id
		username
	  }
	}`

	req, err := http.NewRequest(http.MethodPost, "https://glimesh.tv/api", strings.NewReader(query))
	if err != nil {
		return nil, nil, err
	}

	res, err := api.MakeGlimeshReq(req, true)
	if err != nil {
		return nil, nil, err
	}

	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, nil, err
	}

	resp := &struct {
		Data struct {
			Myself struct {
				ID       string
				Username string
			}
		}
	}{}

	err = json.Unmarshal(data, resp)
	if err != nil {
		return nil, nil, err
	}

	return &resp.Data.Myself.ID, &resp.Data.Myself.Username, nil
}
