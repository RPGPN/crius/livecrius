package main

import (
	"context"
	"fmt"
	"github.com/getsentry/sentry-go"
	"github.com/joho/godotenv"
	"github.com/makasim/sentryhook"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/sirupsen/logrus"
	"gitlab.com/RPGPN/crius/livecrius/settings"
	cc "gitlab.com/RPGPN/criuscommander/v2"
	"gitlab.com/RPGPN/criuscommander/v2/glimesh"
	"gitlab.com/RPGPN/criuscommander/v2/twitch"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func genTokenMap() (map[cc.PlatformType]string, error) {
	twitchToken, err := GetTwitchAccessToken()
	if err != nil {
		return nil, err
	}

	glimeshToken, err := GetGlimeshAccessToken()
	if err != nil {
		return nil, err
	}

	tokens := map[cc.PlatformType]string{
		cc.PlatformTwitch:  fmt.Sprintf("%s@oauth:%s", os.Getenv("TWITCH_NAME"), *twitchToken),
		cc.PlatformGlimesh: *glimeshToken,
	}

	return tokens, nil
}

func init() {
	err := godotenv.Load()
	if err != nil {
		panic("Error loading .env file")
	}

	level := os.Getenv("LOG_LVL")

	if level == "" {
		level = "info"
	}

	intLevel, err := logrus.ParseLevel(level)
	if err != nil {
		panic(err)
	}

	logrus.SetLevel(intLevel)
}

func main() {
	// connect to sentry
	err := sentry.Init(sentry.ClientOptions{
		Dsn:              os.Getenv("SENTRY_DSN"),
		Environment:      os.Getenv("SENTRY_ENV"),
		AttachStacktrace: true,
	})
	defer sentry.Flush(2 * time.Second)

	// first hook logrus & sentry
	if err != nil {
		panic(err)
	}

	sentryHook := sentryhook.New([]logrus.Level{logrus.PanicLevel, logrus.FatalLevel, logrus.ErrorLevel})

	logrus.AddHook(sentryHook)

	lcSettings, err := settings.Setup()
	if err != nil {
		panic(err)
	}

	api := APIFuncs{cache: lcSettings.GetRedisCache()}

	tokens, err := genTokenMap()
	if err != nil {
		panic(err)
	}

	// now get the channels to autojoin
	ajTwitch, err := lcSettings.GetChannels(cc.PlatformTwitch)
	if err != nil {
		panic(err)
	}

	ajTwitch = append(ajTwitch, os.Getenv("TWITCH_NAME"))

	ajGlimesh, err := lcSettings.GetChannels(cc.PlatformGlimesh)
	if err != nil {
		panic(err)
	}

	// get our glimesh ID
	glimID, glimName, err := api.glimeshWhoAmI()
	if err != nil {
		panic(err)
	}

	glimIntChannel, err := api.GlimeshGetChannelID(*glimName)
	if err != nil {
		panic(err)
	}

	ajGlimesh = append(ajGlimesh, *glimIntChannel)

	ctx := context.WithValue(context.Background(), "API", api)
	ctx = context.WithValue(ctx, "settings", lcSettings)

	cmder, err := cc.NewCommander(&cc.CCConfig{
		Prefix:          os.Getenv("PREFIX"),
		Tokens:          tokens,
		PluginLoaders:   cc.DefaultLoaderChain,
		AutoloadPlugins: false,
		Platforms:       []cc.Platform{twitch.New(), glimesh.New()},
		AutoJoinChannels: map[cc.PlatformType][]interface{}{
			cc.PlatformTwitch:  ajTwitch,
			cc.PlatformGlimesh: ajGlimesh,
		},
		OtherConfig: map[string]interface{}{
			"GlimeshID":  *glimID,
			"TwitchName": os.Getenv("TWITCH_NAME"),
			"URL":        "https://gitlab.com/RPGPN/crius/livecrius",
			"changelogs": "https://crius.rpgpn.co.uk/changelogs/livecrius/",
		},
	}, ctx)
	if err != nil {
		panic(err)
	}

	cmder.OnError = func(err error) {
		logrus.Error(err)
	}

	err = loadPlugins(cmder)
	if err != nil {
		panic(err)
	}

	err = cmder.Open()
	if err != nil {
		panic(err)
	}

	http.Handle("/metrics", promhttp.Handler())
	http.ListenAndServe(":8080", nil)

	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	cmder.Close()
}
