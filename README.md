# LiveCrius

## Running
We publish Docker images for this repo. They're currently ARM only, as that's the only place we currently run the bot.

The recommended approach is by using Docker Compose.

## Building
The steps here are the same no matter which option you choose. 

1. Install [Go](https://golang.org). This project requires go 1.16 or higher
2. Set up a postgres database
3. Get [Twitch](https://dev.twitch.tv) and [Glimesh](https://glimesh.tv/users/settings/applications) API keys
4. `git clone` this repo
5. `go build .`
6. Provide the required [Environment Variables](#environment-variables)
7. Follow the steps shown in the console to link your bot to a Twitch and Glimesh account.
   (Note that you need to be able to edit tokens.json to do this, I might implement Vault at some point?)
8. Beep boop.

## Environment Variables
|Name|Value|
|---|---|
|LOG_LVL|Only log entries with this severity or anything above it (see [Logrus Docs](https://pkg.go.dev/github.com/sirupsen/logrus#readme-level-logging))|
|DATABASE_URL|A postgres database URI (i.e. `postgres://[USERNAME]:[PASSWORD]@[HOST]/[DB]`|
|PREFIX|What the bot should expect commands to be prefixed with (i.e. `!source`, where `!` are the prefix)|
|TWITCH_SEC|The secret key of the Twitch Application|
|TWITCH_CID|The client ID of the Twitch Application|
|TWITCH_RED_URI|The redirect URI for Twitch to send you to after authenticating|
|TWITCH_NAME|The name of the Twitch account which the bot is signing in to|
|GLIM_SEC|The secret key of the Glimesh Application|
|GLIM_CID|The client ID of the Glimesh Application|
|GLIM_RED_URI|The redirect URI for Glimesh to send you to after authenticating|
|SENTRY_DSN|*Optional* Your sentry DSN for error catching (not setting this disables Sentry)|
|SENTRY_ENV|*Optional* The sentry environment to report|
|CHANNEL_MANAGER|Set to `true` if you want users to be able to have the bot join their channels|
|REDIS_ADDR|The address of the redis server|
|REDIS_USER|The username to connect to redis with|
|REDIS_PASS|The password for the redis user|

## Redis
Why use two connections and two databases?  
I don't really want to mix data that is being used to handle moderation (store/db1) and cache data with TTLs (cache/db0)