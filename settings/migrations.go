package settings

import "github.com/GuiaBolso/darwin"

var migrations = []darwin.Migration{
	{
		Version:     1,
		Description: "Create table CHANNELS",
		Script: `CREATE TABLE channels (
			platform text not null,
			channel_id text not null,
			streamer_id text not null,
			constraint channels_pk
				primary key (platform, channel_id)
		);`,
	},
}
