package settings

import (
	"context"
	"github.com/GuiaBolso/darwin"
	"github.com/go-redis/redis/v8"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/sirupsen/logrus"
	cc "gitlab.com/RPGPN/criuscommander/v2"
	"os"
	"strconv"
)

type Settings struct {
	db         *sqlx.DB
	redisCache *redis.Client
	redisStore *redis.Client
}

func Setup() (*Settings, error) {
	db, err := sqlx.Connect("postgres", os.Getenv("DATABASE_URL"))
	if err != nil {
		return nil, err
	}

	logrus.Info("Connected to the database")

	// do migrations
	driver := darwin.NewGenericDriver(db.DB, darwin.PostgresDialect{})
	migrator := darwin.New(driver, migrations, nil)

	err = migrator.Migrate()
	if err != nil {
		return nil, err
	}

	logrus.Info("Migrations complete!")

	// now connect to redis
	// first the cache (db 0)
	rCache := redis.NewClient(&redis.Options{
		Addr:     os.Getenv("REDIS_ADDR"),
		Username: os.Getenv("REDIS_USER"),
		Password: os.Getenv("REDIS_PASS"),
		DB:       0,
	})

	// ping to be sure
	err = rCache.Ping(context.Background()).Err()
	if err != nil {
		return nil, err
	}

	// and now the store on db 1
	rStore := redis.NewClient(&redis.Options{
		Addr:     os.Getenv("REDIS_ADDR"),
		Username: os.Getenv("REDIS_USER"),
		Password: os.Getenv("REDIS_PASS"),
		DB:       1,
	})

	// and ping store
	err = rStore.Ping(context.Background()).Err()
	if err != nil {
		return nil, err
	}

	logrus.Info("Connected to Redis")

	settings := &Settings{
		db:         db,
		redisCache: rCache,
		redisStore: rStore,
	}

	return settings, nil
}

func (s *Settings) GetChannels(platform cc.PlatformType) ([]interface{}, error) {
	channelsStrings := []interface{}{}

	err := s.db.Select(&channelsStrings, "SELECT channel_id FROM channels WHERE platform=$1", platform.ToString())
	if err != nil {
		return nil, err
	}

	switch platform {
	case cc.PlatformGlimesh:
		{
			nums := []interface{}{}
			for _, channel := range channelsStrings {
				i, err := strconv.Atoi(channel.(string))
				if err != nil {
					return nil, err
				}

				nums = append(nums, i)
			}

			return nums, nil
		}
	case cc.PlatformTwitch:
		return channelsStrings, nil
	}

	return nil, nil
}

func (s *Settings) GetDB() *sqlx.DB {
	return s.db
}

func (s *Settings) GetRedisCache() *redis.Client {
	return s.redisCache
}

func (s *Settings) GetRedisStore() *redis.Client {
	return s.redisStore
}
