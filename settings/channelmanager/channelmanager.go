package channelmanager

import (
	"context"
	"database/sql"
	"errors"
	"github.com/jmoiron/sqlx"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	CriusUtils "gitlab.com/RPGPN/crius-utils"
	"gitlab.com/RPGPN/crius-utils/LiveCrius"
	cc "gitlab.com/RPGPN/criuscommander/v2"
)

var PluginInfo = &cc.PluginJSON{
	Name:               "Channel Manager",
	License:            "MPL-2.0",
	Creator:            "Ben <Ponkey364>",
	URL:                "https://gitlab.com/RPGPN/livecrius/-/tree/hosted/settings/channelmanager",
	Description:        "Channel management plugin",
	SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh,
	Commands: []*cc.PJCommand{
		{
			Name:               "Join Channel",
			Activator:          "join",
			Help:               "Join your channel",
			HandlerName:        "join",
			SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh,
		},
		{
			Name:               "Leave Channel",
			Activator:          "leave",
			Help:               "Leave your channel",
			HandlerName:        "leave",
			SupportedPlatforms: cc.PlatformTwitch | cc.PlatformGlimesh,
		},
	},
}

type channelManager struct {
	db              *sqlx.DB
	pChannelsJoined *prometheus.GaugeVec
}

func initPromData(db *sqlx.DB, pChannelsJoined *prometheus.GaugeVec) error {
	var glimeshJoined int
	err := db.Get(&glimeshJoined, "SELECT COUNT(*) FROM channels WHERE platform='GLIMESH'")
	if errors.Is(err, sql.ErrNoRows) {
		glimeshJoined = 0
	} else if err != nil && err != sql.ErrNoRows {
		return err
	}
	pChannelsJoined.WithLabelValues(cc.PlatformGlimesh.ToString()).Set(float64(glimeshJoined))

	var twitchJoined int
	err = db.Get(&twitchJoined, "SELECT COUNT(*) FROM channels WHERE platform='TWITCH'")
	if errors.Is(err, sql.ErrNoRows) {
		twitchJoined = 0
	} else if err != nil && err != sql.ErrNoRows {
		return err
	}
	pChannelsJoined.WithLabelValues(cc.PlatformTwitch.ToString()).Set(float64(twitchJoined))

	return nil
}

func Setup(ctx context.Context) (map[string]cc.CommandHandler, error) {
	// get stuff we need from ctx
	s := LiveCrius.GetSettings(ctx)

	db := s.GetDB()

	pChannelsJoined := promauto.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: "livecrius",
		Subsystem: "channelmanager",
		Name:      "channels_joined",
		Help:      "Number of channels joined, partitioned by platform",
	}, []string{"platform"})

	err := initPromData(db, pChannelsJoined)
	if err != nil {
		return nil, err
	}

	cm := &channelManager{
		db:              db,
		pChannelsJoined: pChannelsJoined,
	}

	return map[string]cc.CommandHandler{
		"join":  cm.Join,
		"leave": cm.Leave,
	}, nil
}

func (cm *channelManager) Join(m *cc.MessageContext, _ []string) error {
	// we want to get the sender's ID and join their channel
	platform := CriusUtils.GetPlatform(m.Context)

	var channelToJoin interface{}
	userID := m.Author.ID

	switch platform {
	case cc.PlatformTwitch:
		{
			channelToJoin = m.Author.Username

			// first, are we trying to join our own channel? (found this one the hard way)
			if CriusUtils.GetOtherConfig(m.Context)["TwitchName"].(string) == channelToJoin {
				m.Send("really?")
				return nil
			}
		}
	case cc.PlatformGlimesh:
		{
			channelID, err := LiveCrius.GetAPIFuncs(m.Context).GlimeshGetChannelID(m.Author.Username)
			if err != nil {
				return err
			}

			channelToJoin = *channelID

			// first, are we trying to join our own channel? (found this one the hard way)
			if CriusUtils.GetOtherConfig(m.Context)["GlimeshID"].(string) == userID {
				m.Send("really?")
				return nil
			}
		}
	}

	// are we already in the channel?
	//we'll find out by trying to get the streamer_id
	var streamer_id *string
	err := cm.db.Get(&streamer_id, "SELECT streamer_id FROM channels WHERE platform=$1 AND channel_id=$2",
		platform.ToString(), channelToJoin)
	if err != nil && err != sql.ErrNoRows {
		return err
	}

	if streamer_id != nil {
		m.Send("I'm already in your channel")
		return nil
	}

	err = CriusUtils.GetCommander(m.Context).JoinChannel(platform, channelToJoin)
	if err != nil {
		return err
	}

	_, err = cm.db.Exec("INSERT INTO channels (platform, channel_id, streamer_id) VALUES ($1, $2, $3)",
		platform.ToString(), channelToJoin, userID)
	if err != nil {
		return err
	}

	// everything's cool
	m.Send("See you over there!")

	// tell prometheus
	cm.pChannelsJoined.WithLabelValues(platform.ToString()).Inc()

	return nil
}

func (cm *channelManager) Leave(m *cc.MessageContext, _ []string) error {
	// we want to get the sender's ID and leave their channel
	platform := CriusUtils.GetPlatform(m.Context)

	var channelToLeave interface{}
	userID := m.Author.ID

	switch platform {
	case cc.PlatformTwitch:
		channelToLeave = m.Author.Username
	case cc.PlatformGlimesh:
		{
			channelID, err := LiveCrius.GetAPIFuncs(m.Context).GlimeshGetChannelID(m.Author.Username)
			if err != nil {
				return err
			}

			channelToLeave = *channelID

			m.Send("I'll remove your channel from my list but I'll still be here until I restart due to restrictions with Glimesh's API")
		}
	}

	err := CriusUtils.GetCommander(m.Context).LeaveChannel(platform, channelToLeave)
	if err != nil {
		return err
	}

	_, err = cm.db.Exec("DELETE FROM channels WHERE platform=$1 AND channel_id=$2 AND streamer_id=$3",
		platform.ToString(), channelToLeave, userID)
	if err != nil {
		return err
	}

	// everything's cool
	m.Send("Bye for now!")

	// tell prometheus
	cm.pChannelsJoined.WithLabelValues(platform.ToString()).Dec()

	return nil
}
